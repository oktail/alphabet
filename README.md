# alphabet
This i a simple program printing all alphabet letters on arm architecture

Compile :
 arm-linux-gnueabihf-as alphabet.s -o alphabet
 
Run :
 qemu-system-arm -nographic -kernel ./alphabet -m 512 -M vexpress-a15 -cpu cortex-a15
 
Ressources :
 - PL011 documentation : http://infocenter.arm.com/help/topic/com.arm.doc.ddi0183f/DDI0183.pdf
